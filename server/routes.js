const dataCollector = require('./middlewares/dataCollector');

module.exports = app => {

	app.get(`/`, (req, res) => {
		res.sendfile('./public/index.html');
	});

	app.get(`/history`, (req, res) => {
		res.send(dataCollector.getAllQuestions());
	});
};