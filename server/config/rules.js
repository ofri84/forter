module.exports = Object.freeze({
    PROBABILITY_TH: 0.8,
    QUESTION_WORDS: {
        en: ['who', 'where', 'when', 'why', 'what', 'which', 'how']
    },
    QUANTITY_WORDS: {
        en: ['many', 'much', 'very']
    },
    HELP_WORDS: {
        en: ['that', 'is', 'are', 'the', 'have', 'has', 'had', 'it', 'of', 'as', 'to', 'too', 'in', 'at', 'on', 'any', 'about', 'can', 'could', 'should', 'or', 'and', 'were', 'was', 'does', 'do', 'a', 'an']
    }
});