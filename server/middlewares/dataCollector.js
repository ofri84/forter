const crypto = require('crypto');

let questionsIds = [];
let questions = [
    {
        id: crypto.randomBytes(16).toString("hex"),
        question: 'Why did the chicken cross the road?', 
        answer: 'To get to the other side',
        typing: false
    }
];

function addQuestion(question) {
    const questionId = generateQuestionId();
    question['id'] = questionId;
    questions.push(question);
    return question;
}

function generateQuestionId() {
    let questionId = crypto.randomBytes(16).toString("hex");
    while (questionsIds.indexOf(questionId) > -1) {
        questionId = crypto.randomBytes(16).toString("hex");
    }

    questionsIds.push(questionId);
    return questionId;
}

function getAllQuestions() {
    return questions;
}

function addAnswerToQuestion(questionId, answer) {
    let question = questions.filter(q => q.id === questionId)[0];
    if (typeof question !== 'undefined') {
        question['answer'] = answer;
        question['typing'] = false;
        return question;
    } else {
        return null;
    }
}

function setAnswerTyping(questionId, typing) {
    let question = questions.filter(q => q.id === questionId)[0];
    if (typeof question !== 'undefined') {
        question['typing'] = typing;
        return question;
    } else {
        return null;
    }
}

const dataCollector = {
    addQuestion: addQuestion,
    getAllQuestions: getAllQuestions,
    addAnswerToQuestion: addAnswerToQuestion,
    setAnswerTyping: setAnswerTyping
}

module.exports = dataCollector

