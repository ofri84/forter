const rules = require('../config/rules');

let answeredQuestions = new Map();

/**
 * check if we have a similar answered question in the stack
 * similarity should match at least 80% of 'content words'
 * @param {String} question 
 * return prev answer or null
 */
function newQuestion(question) {
    if (answeredQuestions.size === 0) return null;

    let questionKeyArr = stripQuestion(question.replace('?', ''));
    let smartbotAnswer = null;
    for (const [key, value] of answeredQuestions.entries()) {
        const sumDiffWords = key.length <= questionKeyArr.length ? 
            getDiffWords(key, questionKeyArr) : getDiffWords(questionKeyArr, key);
        const maxDenominator = key.length <= questionKeyArr.length ? questionKeyArr.length : key.length;
        const probability = 1 - (sumDiffWords / maxDenominator);
        if (probability >= rules.PROBABILITY_TH) {
            smartbotAnswer = value;
            break;
        }
    }
    return smartbotAnswer;
}

function addAnswer(msg) {
    let questionKeyArr = stripQuestion(msg.question.replace('?', ''));
    answeredQuestions.set(questionKeyArr, msg.answer);
}

/**
 * strip question from 'help words' so we stay only with content words
 * @param {String} question 
 * return array of 'content words'
 */
function stripQuestion(question) {
    return question.toLowerCase().split(" ")
        .filter(word => rules.QUESTION_WORDS.en.indexOf(word) === -1)
        .filter(word => rules.QUANTITY_WORDS.en.indexOf(word) === -1)
        .filter(word => rules.HELP_WORDS.en.indexOf(word) === -1)
}

/**
 * @param {String[]} aQuestion 
 * @param {String[]} bQuestion 
 * return the number of words which exits only in bQuestion 
 */
function getDiffWords(aQuestion, bQuestion) {
    let counter = 0;
    bQuestion.forEach(function(word) {
        if (aQuestion.indexOf(word) === -1)
            counter++;
    })
    return counter;
}

const smartbot = {
    newQuestion: newQuestion,
    addAnswer: addAnswer
}

module.exports = smartbot 