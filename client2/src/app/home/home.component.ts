import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Socket } from 'ng-socket-io';
import swal from 'sweetalert2'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loading = false;
  questions = [];
  myQuestionText = "";
  lastTypedAsnwerQuestionId = null;

  constructor(private api: ApiService, private socket: Socket) { }

  ngOnInit() {
    this.setSocketEvents();
  }

  setSocketEvents() {
    this.socket.on('connect', () => {
      this.fetchQuestionsHistory();
    });

    //my question has been added to stack of questions
    this.socket.on('message_added', (data) => {
      this.myQuestionText = "";
      this._addQuestion(data);
    });

    //new question has been asked by another client
    this.socket.on('new_message', (data) => {
      this._addQuestion(data);
    });

    //my answer has been added to question
    this.socket.on('question_answered', (data) => {
      this._addAnswer(data);
    });

    //new answer has been added to question by another client
    this.socket.on('new_answer', (data) => {
      this._addAnswer(data);
    });
    
    //new answer typing action
    this.socket.on('answer_is_typing', (data) => {
      this._setAnswerTyping(data);
    });
  }

  fetchQuestionsHistory() {
    this.loading = true;
    this.api.getHistory().subscribe(res => {
      this.loading = false;
      this.questions = res;
    }, error => {
      this.loading = false;
      swal({
        title: "Error Occurred",
        text: "could not fetch old messages",
        type: "error"
      });
    });
  }

  _addQuestion(data) {
    this.questions.push(data);
  }

  _addAnswer(data) {
    let question = this.questions.filter(q => q.id === data.id)[0];
		question.answer = data.answer; 
  }

  _setAnswerTyping(data) {
    let question = this.questions.filter(q => q.id === data.id)[0];
		question.typing = data.typing;
  }

  openAnswerTyping(questionId) {
    this.socket.emit('answer_typing', {id: this.lastTypedAsnwerQuestionId, typing: false});
    this.questions.forEach((q) => {
      if (q.id !== questionId)
        q.typeAnswer = false;
      else
        q.typeAnswer = true;
    });
  }

  onAnswerTyping(event, questionId) {
    this.lastTypedAsnwerQuestionId = questionId;
    this.socket.emit('answer_typing', {id: questionId, typing: true});
  }

  onAsnwerStopTyping(event, questionId) {
    //this.socket.emit('answer_typing', {id: questionId, typing: false});
  }

  sendAnswer(answer, questionId) {
    if (!answer) {
      swal("Please provide a good answer!");
      return;
    }
    this.socket.emit('answer', {id: questionId, answer: answer});
  }

  sendQuestion() {
    if (this.myQuestionText === '') {
      swal("You must enter some text...");
      return;
    }
    this.socket.emit('new_question', this.myQuestionText);
  }
}
