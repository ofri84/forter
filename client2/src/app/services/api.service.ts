import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  constructor(private http: Http) { }

  getHistory(): Observable<any> {
    return this.http.get(environment.baseUrl + '/history')
      .map(res => res.json())
      .catch(error => Observable.throw(error.json()));
  } 

}
