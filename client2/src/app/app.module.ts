import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routing } from './app.routing';
import { LoadingModule } from 'ngx-loading';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:3000', options: {}};

import { ApiService } from './services/api.service'

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Routing,
    LoadingModule,
    MDBBootstrapModule.forRoot(),
    SocketIoModule.forRoot(config) 
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
