# Skeleton app with Node.js + express + angular

## Requirements

- [Node(v6.4) and npm](http://nodejs.org)

## Installation

1. `git clone git@bitbucket.org:ofri84/forter.git`
2. `cd forter`
3. `npm install`
4. `cd client2 && npm install`
5. `cd .. && npm start`
6. Go to `http://localhost:4200`
7. Enjoy life.

## Usage

Add routes to server in: `server/routes.js`

Main angular module: `client/app.js`

Main html file: `client/index.html`

## Development

Run `npm run watch` to watch all files in server directory