// App
const app = angular.module('app', []);

// Service to fetch some data..
app.factory('dataServ', ['$http',($http) => {
	return {
		getHistory : () => $http.get('/history')
	}
}]);

// App controller
app.controller('appController', ['$scope','dataServ', ($scope, Data) => {

	$scope.questions = [];
	$scope.newQuestionText = "";

	const socket = io.connect('http://127.0.0.1:3000');
	socket.on('connect', () => {

		Data.getHistory().success(resp => {
			$scope.questions = resp;
		});
	});

	//my question has been added to stack of questions
	socket.on('message_added', (data) => {
		$scope.newQuestionText = "";
		addQuestion(data);
	});

	//new question has been asked by another client
	socket.on('new_message', (data) => {
		addQuestion(data);
	});

	//my answer has been added to question
	socket.on('question_answered', (data) => {
		addAnswer(data);
	});

	//new answer has been added to question by another client
	socket.on('new_answer', (data) => {
		addAnswer(data);
	});

	function addQuestion(data) {
		$scope.$apply(function() {
			$scope.questions.push(data);
		})
	}

	function addAnswer(data) {
		$scope.$apply(function() {
			let question = $scope.questions.filter(q => q.id === data.id)[0];
			question.answer = data.answer; 
		})
	}

	$scope.sendQuestion = function() {
		if ($scope.newQuestionText === '') return;
		socket.emit('new_question', $scope.newQuestionText);
	}

	$scope.sendAnswer = function(questionId, answer) {
		if (!answer) return;
		socket.emit('answer', {id: questionId, answer: answer});
	}
}]);