const express   = require('express');
const app       = express();
const server    = require('http').createServer(app);  
const io        = require('socket.io')(server);
const port      = 3000;

const dataCollector = require('./server/middlewares/dataCollector');
const smartbot  = require('./server/middlewares/smartbot');

app.use(function(req, res, next){
    res.setHeader('Content-Type', 'application/json')
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Headers', '*')
    res.setHeader('Access-Control-Allow-Methods', '*')
    next()
});

app.use(express.static(`${__dirname}/client`)); 		// statics

require(`./server/routes.js`)(app);						// routes

io.on('connection', function(client) {  
    console.log('Client connected...');

    //new question arrived
    client.on('new_question', function(question) {
        if (question.length < 2) return; 

        //check whether samrtbot already knows the answer
        let smartbotAnswer = smartbot.newQuestion(question);
        //add question to stack
        let msg = dataCollector.addQuestion({
                question: question,
                answer: smartbotAnswer,
                typing: false
            })
        //emit message to sender
        client.emit('message_added', msg);
        //broadcast message to all connected clients
        client.broadcast.emit('new_message', msg);
    });

    //a question has been answered
    client.on('answer', function(data) {
        if (typeof data.id === 'undefined' || typeof data.answer === 'undefined') return;

        //add answer to relevant question
        let msg = dataCollector.addAnswerToQuestion(data.id, data.answer);
        if (msg) {
            smartbot.addAnswer(msg);
            //emit answer to sender
            client.emit('question_answered', msg);
            //broadcast answer to all conencted clients
            client.broadcast.emit('new_answer', msg);
        }    
    });

    //someone type an answer
    client.on('answer_typing', function(data) {
        if (typeof data.id === 'undefined') return;
        const typing = typeof data.typing === 'undefined' ? false : data.typing; 
        //set typing action 
        let msg = dataCollector.setAnswerTyping(data.id, data.typing);
        if (msg) {
            //broadcast answer typing action to all conencted clients
            client.broadcast.emit('answer_is_typing', msg);
        }
    });
})

server.listen(port);								    // let the games begin!
console.log(`Web server listening on port ${port}`);
